import sys
import os

print("Parsing en cours...")

#Read all the files in the data directory
for loadedFile in os.listdir("../data"):
    print(loadedFile)

    #Open the current file
    fileVTK = open("../data/"+loadedFile,"r")
    lineread = fileVTK.readline().split()

    #Skipping the entire first section until we reach the next one
    while lineread:
        lineread = fileVTK.readline().split()

    #Skip the first two line of the current section
    lineread = fileVTK.readline().split()
    lineread = fileVTK.readline().split()

    #Preparation of the new .csv file to be written
    arraysize = lineread.__len__()
    newFile = open("../dataParsed/" + loadedFile + ".csv", "a")
    newFile.write("\"Source\",\"Target\"\n")
    newline = ""

    #rewrite the line read on the .csv file
    while lineread:
        for i in range(0,arraysize-1):
            newline = newline + lineread[i] + "," + lineread[i+1] + "\n"

        newFile.write(newline)
        lineread = fileVTK.readline().split()
        arraysize = lineread.__len__()
        newline = ""

    #close files
    fileVTK.close()
    newFile.close()

#End of script when all files have been read
print("Travail terminé...")