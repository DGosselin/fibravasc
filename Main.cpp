#include "Main.h"
#include "View/View3D.h"
#include "View/View2D.h"
#include "Model/Graph2D.h"
#include "Model/Graph3D.h"

using namespace std;

/*
* Declaration of the main method for fibravasc.
* Creates 2 threads, one displaying a 3D graph, the other a 2D graph
*/
int main(int, char* [])
{
	//Lock declaration, if needed for the threads
	std::mutex lock;
	
	//Declaration of the thread managing the 3D display
	std::thread ThreeDThread([&lock](){
		std::string path = "data";
		Graph3D* graph3d = new Graph3D(path);
		
		View3D* view3d = new View3D(graph3d->graphReader());
		view3d->displayGraph();

	});
	
	//Declaration of the thread managing the 2D display
	std::thread TwoDThread([&lock]() {
		std::string path = "dataParsed";
		Graph2D* graph2D = new Graph2D(path);

		View2D* view2d = new View2D(graph2D->graphReader());
		view2d->displayGraph();
	});

	//ThreeDThread.join();
	TwoDThread.join();
	return EXIT_SUCCESS;
}