#pragma once
#include <string>
#include <iostream>
#include <filesystem>
#include <thread>
#include <mutex>
//#include <Python>

namespace fs = std::filesystem;

#include "vtkGraphLayoutView.h"
#include "vtkRandomGraphSource.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkGenericDataObjectReader.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkSmartPointer.h"
#include "vtkActor.h"
#include "vtkRenderer.h"
#include "vtkProperty.h"
#include "vtkPointData.h"
#include "vtkCollapseGraph.h"
#include "vtkGraphLayoutView.h"
#include "vtkRenderedGraphRepresentation.h"
#include "vtkStringToNumeric.h"
#include "vtkMutableUndirectedGraph.h"
#include "vtkSimple2DLayoutStrategy.h"
#include "vtkDelimitedTextReader.h"
#include "vtkTable.h"
#include "vtkTableToGraph.h"
#include "vtkDenseArray.h"
#include "vtkArrayData.h"
#include "vtkAdjacencyMatrixToEdgeTable.h"
#include "vtkEdgeTable.h"
#include "vtkGraphLayout.h"
#include "vtkContextActor.h"
#include "vtkContextTransform.h"
#include "vtkGraphItem.h"
#include "vtkContextScene.h"
#include "vtkRandomLayoutStrategy.h"
#include "vtkContextInteractorStyle.h"
#include "vtkMutableDirectedGraph.h"
#include "vtkImageData.h"
#include "vtkImageSliceMapper.h"
#include "vtkImageSlice.h"
#include "vtkImageProperty.h"
#include "vtkCamera.h"
#include "vtkImageShiftScale.h"
#include "vtkNIFTIImageWriter.h"
#include "vtkNIFTIImageReader.h"
#include "vtkInteractorStyleImage.h"

#include <vtkAutoInit.h> 
VTK_MODULE_INIT(vtkRenderingOpenGL2)

#define VTK_CREATE(type, name) vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

class Main
{
};

