#include "Graph2D.h"

/*
* Declaration of the class representing the model needed to store a renderer representing a 2D Graph representation of the vascular tree.
*/
Graph2D::Graph2D(std::string path) {
	this->setPath(path);
}

std::string Graph2D::getPath(void) {
	return this->path;
}

void Graph2D::setPath(std::string path) {
	this->path = path;
}

/*
* graphReader : Method preparing the renderer for a 2D display
* Input : void
* Output : vtkRenderer* : renderer with the 2D graph loaded
*/
vtkRenderer* Graph2D::graphReader() {
		//std::string path = "dataParsed";
		VTK_CREATE(vtkEdgeTable, edgeTable);
			
		VTK_CREATE(vtkGraphLayout, graphLayout);
		VTK_CREATE(vtkTableToGraph, tableToGraph);
		VTK_CREATE(vtkDelimitedTextReader, csvReader);
		VTK_CREATE(vtkTable, table);

		tableToGraph->SetInputConnection(csvReader->GetOutputPort());
		tableToGraph->AddLinkVertex("Source");
		tableToGraph->AddLinkVertex("Target");
		tableToGraph->AddLinkEdge("Source", "Target");
		tableToGraph->SetDirected(true);

		vtkSmartPointer<vtkMutableUndirectedGraph> g =
			vtkSmartPointer<vtkMutableUndirectedGraph>::New();
		int lastIndex = 0;

		//Parse all the files in the data directory
		for (const auto& entry : fs::directory_iterator(this->getPath())) {

			csvReader->SetFileName(entry.path().string().c_str());
			csvReader->SetStringDelimiter('\"');
			csvReader->SetHaveHeaders(true);
			csvReader->SetDetectNumericColumns(true);
			csvReader->SetOutputPedigreeIds(true);
			csvReader->SetFieldDelimiterCharacters(",");
			csvReader->Update();

			table = csvReader->GetOutput();
			//table->Dump();

			tableToGraph->Update();
			tableToGraph->GetLinkGraph()->Dump();

			//Read all the lines in the loaded file, and add them to the table
			for (int i = 1; i < table->GetNumberOfRows(); i++) {
				while (table->GetValue(i, 0).ToInt() + lastIndex >= g->GetNumberOfVertices() || table->GetValue(i, 1).ToInt() + lastIndex >= g->GetNumberOfVertices()) g->AddVertex();
				//cout << g->GetNumberOfVertices();
				g->AddEdge(table->GetValue(i, 0).ToInt(), table->GetValue(i, 1).ToInt());
			}

			lastIndex = g->GetNumberOfVertices();
			cout << "travail termin�\n----------------------------------------\n";
		}
		//*/



		table = csvReader->GetOutput();

		//*
		VTK_CREATE(vtkRandomLayoutStrategy, layoutStrategy);
		layoutStrategy->SetGraphBounds(0, 800, 0, 800, 0, 0);

		graphLayout->SetInputConnection(tableToGraph->GetOutputPort());
		graphLayout->SetLayoutStrategy(layoutStrategy.GetPointer());
		graphLayout->Update();

		VTK_CREATE(vtkGraphItem, graphItem);
		graphItem->SetGraph(graphLayout->GetOutput());

		VTK_CREATE(vtkContextTransform, trans);
		trans->SetInteractive(true);
		trans->AddItem(graphItem.GetPointer());

		VTK_CREATE(vtkContextActor, actor);
		actor->GetScene()->AddItem(trans.GetPointer());

		VTK_CREATE(vtkRenderer, renderer);
		renderer->AddActor(actor.GetPointer());

		return renderer;
	}