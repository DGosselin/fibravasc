#include "Graph3D.h"

/*
* Declaration of the class representing the model needed to store a renderer representing a 3D Graph representation of the vascular tree.
*/

Graph3D::Graph3D(std::string path) {
	this->path = path;
}

std::string Graph3D::getPath(void) {
	return this->path;
}

void Graph3D::setPath(std::string path) {
	this->path = path;
}

/*
* niftiReader : Method reading the nifti file and loading in into the renderer
* Input : void
* Output : vtkRenderer* : renderer with the nifti file loaded.
*/
vtkRenderer* Graph3D::niftiReader(void) {
	std::string inpath = "niftiData\\wm_packed_to_cortex_parc_bert.nii.gz";

	VTK_CREATE(vtkNIFTIImageReader, reader);
	reader->SetFileName(inpath.c_str());
	reader->Update();
	
	int* size = reader->GetOutput()->GetDimensions();
	double* center = reader->GetOutput()->GetCenter();
	double* spacing = reader->GetOutput()->GetCenter();
	double* vrange = reader->GetOutput()->GetScalarRange();
	double center1 = (center[0], center[1], center[2]);
	double center2 = (center[0], center[1], center[2]);
	if (size[2] % 2 == 1) center1 = (center[0], center[1], center[2] + 0.5 * spacing[2]);
	if (size[0] % 2 == 1) center2 = (center[0] + 0.5 * spacing[0], center[1], center[2]);

	VTK_CREATE(vtkImageSliceMapper, map1);
	map1->BorderOn();
	map1->SliceAtFocalPointOn();
	map1->SliceFacesCameraOn();
	map1->SetInputConnection(reader->GetOutputPort());

	VTK_CREATE(vtkImageSliceMapper, map2);
	map2->BorderOn();
	map2->SliceAtFocalPointOn();
	map2->SliceFacesCameraOn();
	map2->SetInputConnection(reader->GetOutputPort());

	VTK_CREATE(vtkImageSlice, slice1);
	slice1->SetMapper(map1);
	slice1->GetProperty()->SetColorWindow(vrange[1] - vrange[0]);
	slice1->GetProperty()->SetColorLevel(0.5 * (vrange[0] + vrange[1]));

	VTK_CREATE(vtkImageSlice, slice2);
	slice2->SetMapper(map2);
	slice2->GetProperty()->SetColorWindow(vrange[1] - vrange[0]);
	slice2->GetProperty()->SetColorLevel(0.5 * (vrange[0] + vrange[1]));

	double ratio = size[0] * 1.0 / (size[0] + size[2]);

	VTK_CREATE(vtkRenderer, ren1);
	ren1->SetViewport(0, 0, ratio, 1.0);

	VTK_CREATE(vtkRenderer, ren2);
	ren2->SetViewport(ratio, 0.0, 1.0, 1.0);

	//ren1->AddViewProp(slice1);
	//ren2->AddViewProp(slice2);

	VTK_CREATE(vtkCamera, cam1);
	cam1 = ren1->GetActiveCamera();
	cam1->ParallelProjectionOn();
	cam1->SetParallelScale(0.5 * spacing[1] * size[1]);
	//cam1->SetFocalPoint(center1[0], center1[1], center1[2]);
	//cam1->SetPosition(center1[0], center1[1], center1[2]-100);

	VTK_CREATE(vtkCamera, cam2);
	cam2 = ren2->GetActiveCamera();
	cam2->ParallelProjectionOn();
	cam2->SetParallelScale(0.5 * spacing[1] * size[1]);
	//cam2->SetFocalPoint(center2[0], center2[1], center2[2]);
	//cam2->SetPosition(center2[0], center2[1], center2[2] - 100);

	return ren1;
}


/*
* graphReader : Method preparing the renderer for a 3D display
* Input : void
* Output : vtkRenderer* : renderer with the 3D graph loaded
*/
vtkRenderer* Graph3D::graphReader(void) {

	vtkSmartPointer<vtkRenderer> renderer =
		vtkSmartPointer<vtkRenderer>::New();
	float i = 0.0;
	
	renderer = this->niftiReader();

	// Get all data from the files
	cout << "plot";

	for (const auto& entry : fs::directory_iterator(this->getPath().c_str())) {

		vtkSmartPointer<vtkGenericDataObjectReader> reader =
			vtkSmartPointer<vtkGenericDataObjectReader>::New();
		reader->SetFileName(entry.path().string().c_str());
		reader->Update();

		//Checking file compatibility
		if (reader->IsFilePolyData())
		{
			vtkPolyData* output = reader->GetPolyDataOutput();

			vtkSmartPointer<vtkPolyDataMapper> mapper =
				vtkSmartPointer<vtkPolyDataMapper>::New();
			mapper->SetInputData(output);

			vtkSmartPointer<vtkActor> actor =
				vtkSmartPointer<vtkActor>::New();

			actor->SetMapper(mapper);
			actor->GetProperty()->SetPointSize(10);
			actor->GetProperty()->SetLineWidth(4);

			vtkAbstractArray* rayons = output->GetPointData()->GetAbstractArray("Radius");
			vtkUnsignedCharArray* colors1 = vtkUnsignedCharArray::New();

			colors1->SetName("colors1");


			for (int i; i = 0; i == rayons->GetSize())
			{
				//rayons->set;

				//TODO convertir code en Java
				// <3 IntelliJ <3
			}


			output->GetPointData();

			actor->GetProperty()->SetColor(i, i, i);

			i += .1;
			renderer->AddActor(actor);

		}
	}

	return renderer;
}