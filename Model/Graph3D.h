#pragma once

#include <string>
#include <iostream>
#include <filesystem>
#include <thread>
#include <mutex>
//#include <Python>

#include "vtkGraphLayoutView.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkGenericDataObjectReader.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkSmartPointer.h"
#include "vtkActor.h"
#include "vtkRenderer.h"
#include "vtkProperty.h"
#include "vtkPointData.h"
#include "vtkCollapseGraph.h"
#include "vtkGraphLayoutView.h"
#include "vtkRenderedGraphRepresentation.h"
#include "vtkStringToNumeric.h"
#include "vtkMutableUndirectedGraph.h"
#include "vtkSimple2DLayoutStrategy.h"
#include "vtkDelimitedTextReader.h"
#include "vtkTable.h"
#include "vtkTableToGraph.h"
#include "vtkDenseArray.h"
#include "vtkArrayData.h"
#include "vtkAdjacencyMatrixToEdgeTable.h"
#include "vtkEdgeTable.h"
#include "vtkGraphLayout.h"
#include "vtkContextActor.h"
#include "vtkContextTransform.h"
#include "vtkGraphItem.h"
#include "vtkContextScene.h"
#include "vtkRandomLayoutStrategy.h"
#include "vtkContextInteractorStyle.h"
#include "vtkMutableDirectedGraph.h"
#include "vtkImageData.h"
#include "vtkImageSliceMapper.h"
#include "vtkImageSlice.h"
#include "vtkImageProperty.h"
#include "vtkCamera.h"
#include "vtkImageShiftScale.h"
#include "vtkNIFTIImageWriter.h"
#include "vtkNIFTIImageReader.h"
#include "vtkInteractorStyleImage.h"

#include <vtkAutoInit.h> 

namespace fs = std::filesystem;

#define VTK_CREATE(type, name) vtkSmartPointer<type> name = vtkSmartPointer<type>::New()


/*
* Header of the class representing the model needed to store a renderer representing a 3D Graph representation of the vascular tree.
*/
class Graph3D {
private:
	std::string path; //Path of the data files
	vtkRenderer* niftiReader(void); //Method reading the nifti file and loading in into the renderer
public:
	Graph3D(std::string path);

	//Getter/Setter for the path
	std::string getPath(void);
	void setPath(std::string);
	
	vtkRenderer* graphReader(void); //Method preparing the renderer for a 3D display
};