Fibravasc is a work in progress C++ program allowing the user to display a 3D and 2D representation of a cerebral vascular tree, categorized in zones depending on the location within the brain.

Requirements:

- A C++ compiler (MSVC, and in general the latest iteration of Visual Studio, is recommended to work on this program)
- Cmake. Download link : https://cmake.org/download/
- The Visualtion ToolKit (VTK) version 8.2.0. Download link : https://vtk.org/download/
- .vtk files containing the vascular tree. Exemples are provided in the directory "data"
- at least one NIFTI file containing the categorisation of a brain. Exemples are provided in the directory "NiftiData"


Repository Contents:

- All the files needed to compile this program with Visual Studio 2019 and Cmake
- Model & View : 
    Respectively the repository for the models and the views used in the program
    
- data: 
    Contains the .vtk files representing a vascular tree. 
    Used for the 3D display of the tree

- NiftiData: 
    Contains the NIFTI files representing the categorisation of a brain. 
    Used to categorized each part of the 3D tree

- GraphParsing: 
    Contains a Python script and a virtual environment to convert .vtk files into .csv files. 
    This script needs to be run prior to the 2D display.
    
- dataParsed: 
    Contains all the .csv files converted by GraphParsing. 
    Used in the 2D Display of the tree