#include"View2D.h"

/*
* Declaration of the method of the class in charge of displaying the loaded 2D graph renderer
*/

View2D::View2D(vtkRenderer* renderer) {
	this->renderer = renderer;
}

void View2D::setRenderer(vtkRenderer* renderer) {
	this->renderer = renderer;
}

vtkRenderer* View2D::getRenderer(void) {
	return this->renderer;
}

/*
* displayGraph : method displaying the 3D graph loaded in the renderer.
* Input : none
* Output : none
*/
void View2D::displayGraph() {

	VTK_CREATE(vtkRenderWindow, renderWindow);
	renderWindow->SetSize(800, 800);
	renderWindow->AddRenderer(this->getRenderer());

	VTK_CREATE(vtkContextInteractorStyle, interactorStyle);
	//interactorStyle->SetScene(actor->GetScene());

	VTK_CREATE(vtkRenderWindowInteractor, interactor);
	interactor->SetInteractorStyle(interactorStyle.GetPointer());
	interactor->SetRenderWindow(renderWindow.GetPointer());
	renderWindow->SetLineSmoothing(true);

	cout << "rendering started\n";
	renderWindow->Render();
	cout << "Rendering done\n";

	//graphItem->StartLayoutAnimation(interactor.GetPointer());
	interactor->Start();
}
