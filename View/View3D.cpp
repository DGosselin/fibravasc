#include"View3D.h"

/*
* Declaration of the methods of the class in charge of displaying the loaded 3D graph renderer
*/

View3D::View3D(vtkRenderer* renderer) {
	this->setRenderer(renderer);
}

void View3D::setRenderer(vtkRenderer* renderer) {
	this->renderer = renderer;
}

vtkRenderer* View3D::getRenderer(void) {
	return this->renderer;
}

/*
* displayGraph : method displaying the 3D graph loaded in the renderer.
* Input : none
* Output : none
*/
void View3D::displayGraph() {
	VTK_CREATE(vtkRenderWindow, renwin);
	//renwin->SetSize((size[0] + size[2]), size[1]);
	renwin->AddRenderer(this->getRenderer());

	VTK_CREATE(vtkRenderWindowInteractor, interactor);
	//interactor->SetInteractorStyle(style);
	renwin->SetInteractor(interactor);
	renwin->GetInteractor()->Initialize();
	renwin->Render();
	renwin->GetInteractor()->Start();
}
