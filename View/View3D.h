#pragma once

#include "vtkRenderer.h"
#include "vtkGraphLayoutView.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkGenericDataObjectReader.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkSmartPointer.h"
#include "vtkActor.h"
#include "vtkRenderer.h"
#include "vtkProperty.h"
#include "vtkPointData.h"
#include "vtkCollapseGraph.h"
#include "vtkGraphLayoutView.h"
#include "vtkRenderedGraphRepresentation.h"
#include "vtkStringToNumeric.h"
#include "vtkMutableUndirectedGraph.h"
#include "vtkSimple2DLayoutStrategy.h"
#include "vtkDelimitedTextReader.h"
#include "vtkTable.h"
#include "vtkTableToGraph.h"
#include "vtkDenseArray.h"
#include "vtkArrayData.h"
#include "vtkAdjacencyMatrixToEdgeTable.h"
#include "vtkEdgeTable.h"
#include "vtkGraphLayout.h"
#include "vtkContextActor.h"
#include "vtkContextTransform.h"
#include "vtkGraphItem.h"
#include "vtkContextScene.h"
#include "vtkRandomLayoutStrategy.h"
#include "vtkContextInteractorStyle.h"
#include "vtkMutableDirectedGraph.h"
#include "vtkImageData.h"
#include "vtkImageSliceMapper.h"
#include "vtkImageSlice.h"
#include "vtkImageProperty.h"
#include "vtkCamera.h"
#include "vtkImageShiftScale.h"
#include "vtkNIFTIImageWriter.h"
#include "vtkNIFTIImageReader.h"
#include "vtkInteractorStyleImage.h"

#include <vtkAutoInit.h> 

#define VTK_CREATE(type, name) vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

/*
* Header of the class in charge of displaying the loaded 3D graph renderer.
* Input : vtkRenderer* renderer : Renderer with a 3D loaded
*/
class View3D {
private:
	vtkRenderer* renderer; //renderer with a 3D graph loaded

public:
	View3D(vtkRenderer* renderer);

	//Getter/Setter for renderer
	vtkRenderer* getRenderer(void);
	void setRenderer(vtkRenderer* renderer);

	void displayGraph(void); //Method displaying the 3D graph
};